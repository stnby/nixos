# configuration.nix
# author: Stnby
# ---------------------
# potatobox: 2019-03-22
# thinkpod:  2019-09-09

{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  # Bootloader
  boot = {
    loader = {
      timeout = 1;
      systemd-boot.enable = true;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
    };
    #kernelPackages = pkgs.linuxPackages_latest;
    kernelPackages = pkgs.linuxPackages_5_1;
    extraModulePackages = [ config.boot.kernelPackages.wireguard ];
  };

  # Networking
  networking = {
    hostName = "thinkpod";
    networkmanager.enable = true;
    nameservers = [ "1.1.1.1" "1.0.0.1" ];
    firewall = {
      allowedTCPPorts = [ 22 80 443 1337 1234 6969 ];
      #allowedUDPPorts = [ ... ];
    };
  };


  # Internationalisation
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Time zone.
  time.timeZone = "Europe/Vilnius";

  # Allow nonFree packages
  nixpkgs.config.allowUnfree = true;

  # Packages for system profile
  environment.systemPackages = with pkgs; [
    curl wget vim sxhkd git htop nmap feh conky neofetch dmenu pciutils psmisc compton
    firefox tdesktop rofi flameshot jq xterm networkmanagerapplet bat torsocks vlc
    obs-studio mpv ffmpeg redshift gimp virtualbox pavucontrol exa acpilight libreoffice
    unzip wireguard wireguard-tools iw dunst libnotify dejavu_fonts efibootmgr
  ];

  # Services
  services = {
    openssh.enable = true;
    tor.enable = true;
    xserver = {
      enable = true;
      #videoDrivers = [ "amdgpu" ]; # Included in 5.x.
      layout = "us,lt";
      xkbOptions = "grp:ctrl_alt_toggle";
      libinput.enable = true;
      deviceSection = ''
        Option "TearFree" "true"
      '';
      windowManager.bspwm.enable = true;
      displayManager.lightdm = {
        background = "/mnt/data/bg.png";
        autoLogin = {
          enable = false;
          user = "stnby";
        };
      };
    };
    # Udev rule to adjust backlight without root.
    udev = {
      extraRules = ''
        SUBSYSTEM=="backlight", ACTION=="add", RUN+="${pkgs.coreutils}/bin/chgrp video /sys/class/backlight/%k/brightness", RUN+="${pkgs.coreutils}/bin/chmod g+w /sys/class/backlight/%k/brightness"
      '';
    };
  };

  # ADB
  programs.adb.enable = true;

  # Everyones loved language.
  programs.java.enable = true;

  # VirtualBox
  virtualisation.virtualbox.host.enable = true;

  # Enable sound.
  sound.enable = true;

  # Hardware
  hardware = {
    pulseaudio = {
      enable = true;
      #support32Bit = true;
    };
    #opengl.driSupport32Bit = true;
  };

  # Users
  users.users.stnby = {
    isNormalUser = true;
    extraGroups = [ "video" "wheel" "networkmanager" "vboxusers" "adbusers" ];
    #packages = [ ... ];
    initialPassword = "YarakCukSikVap";
    uid = 1000;
  };

  # Mount data partition
  fileSystems."/mnt/data" = {
    device = "/dev/disk/by-label/DATA";
    fsType = "ext4";
  };

  system.stateVersion = "19.03";
}